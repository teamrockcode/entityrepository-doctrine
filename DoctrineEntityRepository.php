<?php

namespace TeamRock\EntityRepository\Doctrine;

use Doctrine\ORM\EntityRepository;
use TeamRock\Intefaces\EntityRepository\EntityRepository AS EntityRepositoryInterface;

class DoctrineEntityRepository extends EntityRepository implements EntityRepositoryInterface
{
    /**
     * @return array All the objects in this repository
     */
    public function fetchAll()
    {
        return $this->findAll();
    }

    /**
     * @param $identifier
     * @return object The object with identifier $identifier
     */
    public function fetchOneByIdentifier($identifier)
    {
        return $this->find($identifier);
    }

    /**
     * @param array $criteria
     * @param array $sortBy
     * @param null $limit
     * @param int $offset
     * @return array The objects that match the criteria
     */
    public function findManyUsingCriteria(array $criteria, array $sortBy = array(), $limit = null, $offset = 0)
    {
        return $this->findBy($criteria, $sortBy, $limit,$offset);
    }

    /**
     * @param $object
     */
    public function save($object)
    {
        $this->getEntityManager()->persist($object);
    }

    /**
     * @param $object
     */
    public function delete($object)
    {
        $this->getEntityManager()->remove($object);
    }

    /**
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }
}
